import { Component, ElementRef, ViewChild } from '@angular/core';
import { Content, NavController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   // Retrieve all template reference variables
   @ViewChild(Content) content : Content;
   @ViewChild('panel1') panel1 : ElementRef;
   @ViewChild('panel2') panel2 : ElementRef;
   @ViewChild('panel3') panel3 : ElementRef;
   @ViewChild('panel4') panel4 : ElementRef;
   @ViewChild('panel5') panel5 : ElementRef;
   @ViewChild('panel6') panel6 : ElementRef;
   @ViewChild('panel7') panel7 : ElementRef;




   /**
    * @name panelPos1
    * @type {number}
    * @public
    * @description     Object for storing the y-axis position of the first 'page'
    */
   public panelPos1 : number;
   public panelPos2 : number;
   public panelPos3 : number;
   public panelPos4 : number;
   public panelPos5 : number;
   public panelPos6 : number;
   public panelPos7 : number;




   /**
    * @name panels
    * @type {number}
    * @public
    * @description     Array for storing each 'page' (which will be created dynamically
                       inside the template view)
    */
   public panels 	  : any     = [1,2,3,4,5,6,7];




   is_viewing_mobile:boolean;

   constructor(public platform: Platform, public navCtrl: NavController) {
       if(platform.height() >= platform.width()) this.is_viewing_mobile = true;
       else this.is_viewing_mobile = false;
       window.onresize = (e) =>{
         if(platform.height() > platform.width()) this.is_viewing_mobile = true;
         else this.is_viewing_mobile = false;
       }
   }




   /**
    * Determine the y-axis position for each 'page' once the view has loaded
    * and assign these values to properties for later reference
    *
    * @public
    * @method ionViewDidLoad
    * @return {none}
    */
   ionViewDidLoad() : void
   {
      this.panelPos1 = this.panel1.nativeElement.getBoundingClientRect().top,
      this.panelPos2 = this.panel2.nativeElement.getBoundingClientRect().top,
      this.panelPos3 = this.panel3.nativeElement.getBoundingClientRect().top,
      this.panelPos4 = this.panel4.nativeElement.getBoundingClientRect().top,
      this.panelPos5 = this.panel5.nativeElement.getBoundingClientRect().top;
      this.panelPos6 = this.panel6.nativeElement.getBoundingClientRect().top,
      this.panelPos7 = this.panel7.nativeElement.getBoundingClientRect().top;
   }




   /**
    * Scrolls to a specified point on the x/y axes
    *
    * @public
    * @method scrollTo
    * @param x            {Number}         The amount of pixels to scroll on the x axis
    * @param y            {Number}         The amount of pixels to scroll on the y axis
    * @param duration     {Number}         The scroll duration (in milliseconds)
    * @return {none}
    */
   scrollTo(x         : number,
            y         : number,
            duration  : number) : void
   {
     if(this.is_viewing_mobile) this.content.scrollTo(x, y-200, duration);
     else this.content.scrollTo(x, y, duration)
   }




   /**
    * Scrolls to the specified 'page'
    *
    * @public
    * @method scrollToPanel
    * @param num            {Number}         The number of the 'page' to scroll to
    * @return {none}
    */
   scrollToPanel(num : number) : void
   {
      switch(num)
      {
         case 1:
           this.scrollTo(0, this.panelPos1, 750);
         break;


         case 2:
           this.scrollTo(0, this.panelPos2, 750);
         break;

         case 3:
           this.scrollTo(0, this.panelPos3, 750);
         break;

         case 4:
           this.scrollTo(0, this.panelPos4, 750);
         break;

         case 5:
           this.scrollTo(0, this.panelPos5, 750);
         break;

         case 6:
           this.scrollTo(0, this.panelPos6, 750);
         break;

         case 7:
           this.scrollTo(0, this.panelPos7, 750);
         break;
      }

   }




   /**
    * Scrolls to the top of the content component area
    *
    * @public
    * @method scrollTop
    * @return {none}
    */
   scrollToTop() : void
   {
      this.content.scrollToTop(750);
   }




   /**
    * Scrolls to the bottom of the content component area
    *
    * @public
    * @method scrollTop
    * @return {none}
    */
   scrollToBottom(): void
   {
      this.content.scrollToBottom(750);
   }


}
